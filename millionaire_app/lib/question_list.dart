import 'package:flutter/material.dart';
import './question_list_elem.dart';

class QuestionList extends StatelessWidget {
  final questionId;

  QuestionList(this.questionId);

  @override
  Widget build(BuildContext context) {
    if (true) {
      return AlertDialog(
        backgroundColor: Colors.indigo[700],
        title: Text(
          'Questions List',
          style: TextStyle(color: Colors.white),
        ),
        content: Column(
          children: <Widget>[
            (questionId == 11)
                ? QuestionListElem('12.  1 000 000 PLN', Colors.orange[800])
                : QuestionListElem('12.  1 000 000 PLN', Colors.black),

            (questionId == 10)
                ? QuestionListElem('11.  500 000 PLN', Colors.orange[800])
                : QuestionListElem('11.  500 000 PLN', Colors.black),

            (questionId == 9)
                ? QuestionListElem('10.  250 000 PLN', Colors.orange[800])
                : QuestionListElem('10.  250 000 PLN', Colors.black),

            (questionId == 8)
                ? QuestionListElem('9.   125 000 PLN', Colors.orange[800])
                : QuestionListElem('9.   125 000 PLN', Colors.black),

            (questionId == 7)
                ? QuestionListElem('8.   75 000 PLN', Colors.orange[800])
                : QuestionListElem('8.   75 000 PLN', Colors.black),

            (questionId == 6)
                ? QuestionListElem('7.   40 000 PLN', Colors.orange[800])
                : QuestionListElem('7.   40 000 PLN', Colors.black),

            (questionId == 5)
                ? QuestionListElem('6.   20 000 PLN', Colors.orange[800])
                : QuestionListElem('6.   20 000 PLN', Colors.black),

            (questionId == 4)
                ? QuestionListElem('5.   10 000 PLN', Colors.orange[800])
                : QuestionListElem('5.   10 000 PLN', Colors.black),

            (questionId == 3)
                ? QuestionListElem('4.   5000 PLN', Colors.orange[800])
                : QuestionListElem('4.   5000 PLN', Colors.black),

            (questionId == 2)
                ? QuestionListElem('3.   2000 PLN', Colors.orange[800])
                : QuestionListElem('3.   2000 PLN', Colors.black),

            (questionId == 1)
                ? QuestionListElem('2.   1000 PLN', Colors.orange[800])
                : QuestionListElem('2.   1000 PLN', Colors.black),

            (questionId == 0)
                ? QuestionListElem('1.   500 PLN', Colors.orange[800])
                : QuestionListElem('1.   500 PLN', Colors.black),
          ],
        ),
        actions: <Widget>[
          MaterialButton(
            elevation: 5.0,
            child: Text(
              'Back to game',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ],
      );
    }
  }
}
