import 'package:flutter/material.dart';

class DisabledHelp extends StatelessWidget {

  final Icon content;
  final Function disableHandle;

  DisabledHelp(this.content, this.disableHandle);


  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.grey[600],
      textColor: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
          side: BorderSide(color: Colors.white)),
      child: content,
      onPressed: disableHandle,
    );

  }
}