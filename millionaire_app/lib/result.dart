import 'package:flutter/material.dart';
import 'package:millionaireapp/main.dart';
import 'menu_button.dart';

class Result extends StatelessWidget {
  final int prize;

  Result(this.prize);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Who wants to be a millionaire?'),
        backgroundColor: Colors.indigo[900],
      ),
      body: Container(
        color: Colors.indigo[700],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Text(
                'Congratulations!\nYou won $prize PLN!',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(height: 15),
            MenuButton('Back to menu', AppHome())
          ],
        ),
      ),
    );
  }
}
