import 'package:flutter/material.dart';

class ResignButton extends StatelessWidget {
  final Function resignHandle;

  ResignButton(this.resignHandle);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        color: Colors.red[900],
        onPressed: resignHandle,
        child: Text(
            'Resign',
            style: TextStyle(color: Colors.white),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.white)),
      ),
    );
  }
}