import 'package:flutter/material.dart';
import 'package:millionaireapp/answer.dart';
import 'package:millionaireapp/phone_dialog.dart';
import './help_button.dart';
import './questions_data.dart';
import 'dart:math';
import 'result.dart';
import 'question_text.dart';
import './disabled_help.dart';
import './audience_answer.dart';
import './resign.dart';
import './question_list.dart';

class Game extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _GameState();
  }
}

class _GameState extends State<Game> {
  final _questions = questionsSets[new Random().nextInt(questionsSets.length)];
  var _questionIndex = 0;
  var prize = 0;
  bool _fiftyFiftyAvailable = true;
  bool _phoneAvailable = true;
  bool _publicAvailable = true;

  final prizes = [0, 500, 1000, 2000, 5000, 10000, 20000, 40000, 75000, 125000,
    250000, 500000, 1000000
  ];

  var _disabledAnswers = [];

  void _answer(String choice) {
    String correct = _questions[_questionIndex]['correct'];

    if (choice == correct) {
      setState(() {
        _questionIndex++;
      });
      _disabledAnswers = [];
    } else {
      if (_questionIndex < 2) {
        _questionIndex = 0;
      } else if (_questionIndex < 7) {
        _questionIndex = 2;
      } else if (_questionIndex < 12) {
        _questionIndex = 7;
      }
      prize = prizes[_questionIndex];
      Navigator.pop(context);
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => Result(prize)));
    }
  }

  void _fiftyFifty() {
    String correct = _questions[_questionIndex]['correct'];
    List<String> answers = _questions[_questionIndex]['answer'];
    List<String> incorrect = [];

    for (String elem in answers) {
      if (elem != correct) {
        incorrect.add(elem);
      }
    }

    String notDisabled = incorrect[new Random().nextInt(incorrect.length)];
    incorrect.remove(notDisabled);

    int disable1 = answers.indexOf(incorrect[0]);
    int disable2 = answers.indexOf(incorrect[1]);

    setState(() {
      _disabledAnswers = [disable1, disable2];
    });

    setState(() {
      _fiftyFiftyAvailable = false;
    });
  }

  _phone(BuildContext context) {
    setState(() {
      _phoneAvailable = false;
    });

    List<String> answers = _questions[_questionIndex]['answer'];
    List<String> possibleAnswers = [];
    String correct = _questions[_questionIndex]['correct'];
    String phoneAnswer = '';

    int threshold = 85;
    Random random = new Random();
    int randomNumber = random.nextInt(100);

    for (var elem in answers) {
      if (!_disabledAnswers.contains(answers.indexOf(elem))) {
        possibleAnswers.add(elem);
      }
    }

    possibleAnswers.remove(correct);
    String wrongAnswer = possibleAnswers[0];

    if (randomNumber < threshold) {
      phoneAnswer = 'Hello, I\'m sure, that correct answer is $correct';
    } else {
      phoneAnswer =
          'Hello, unfortunettly I don\'t know. But i think that correct is $wrongAnswer';
    }

    return showDialog(
        context: context,
        builder: (context) {
          return PhoneDialog(phoneAnswer);
        });
  }

  _public(BuildContext context) {
    setState(() {
      _publicAvailable = false;
    });

    String correct = _questions[_questionIndex]['correct'];
    List<String> answers = _questions[_questionIndex]['answer'];
    List<int> votes = [0, 0, 0, 0];
    List<int> percents = [0, 0, 0, 0];
    int votesSum = 0;
    List<int> indexToVote = [0, 1, 2, 3];

    if (_disabledAnswers.length == 2) {
      indexToVote.remove(_disabledAnswers[0]);
      indexToVote.remove(_disabledAnswers[1]);
    }

    Random random = new Random();

    votes[answers.indexOf(correct)] = random.nextInt(40) + 60;
    indexToVote.remove(answers.indexOf(correct));

    for (var elem in indexToVote) {
      votes[elem] = random.nextInt(40) + 20;
    }

    for (var vote in votes) {
      votesSum += vote;
    }

    for (int i = 0; i < 4; i++) {
      percents[i] = (votes[i] / votesSum * 100).round();
    }

    return showDialog(
        context: context,
        builder: (context) {
          return AudienceAnswer(
              percents[0], percents[1], percents[2], percents[3]);
        });
  }

  _showList(BuildContext context) {

    return showDialog(
      context: context,
      builder: (context) {
        return QuestionList(_questionIndex);
      }
    );
  }

  void _disable() {}

  void _resign() {
    prize = prizes[_questionIndex];
    Navigator.pop(context);
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => Result(prize)));
  }

  @override
  Widget build(BuildContext context) {
    return _questionIndex < 12
        ? Scaffold(
            backgroundColor: Colors.indigo[700],
            appBar: AppBar(
              title: Center(
                child: Text('Who wants to be a Millionaire?'),
              ),
              backgroundColor: Colors.indigo[900],
            ),
            body: Column(
              children: <Widget>[
                RaisedButton(
                  color: Colors.indigo[900],
                  child: Text('Show questions list', style: TextStyle(color: Colors.white, fontSize: 20),),
                  onPressed: () => _showList(context),
                ),
                Container(
                  color: Colors.black,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 7,
                        child: Image(
                          image: AssetImage('images/game_background.png'),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Column(
                          children: <Widget>[
                            (_phoneAvailable == true)
                                ? HelpButton(
                                    content: Icon(Icons.phone),
                                    functionHandle: () {
                                      _phone(context);
                                    },
                                  )
                                : DisabledHelp(Icon(Icons.phone), _disable),
                            (_publicAvailable == true)
                                ? HelpButton(
                                    content: Icon(Icons.people),
                                    functionHandle: () {
                                      _public(context);
                                    },
                                  )
                                : DisabledHelp(Icon(Icons.people), _disable),
                            (_fiftyFiftyAvailable == true)
                                ? HelpButton(
                                    content: Icon(const IconData(0xe900,
                                        fontFamily: 'percent')),
                                    functionHandle: _fiftyFifty,
                                  )
                                : DisabledHelp(
                                    Icon(const IconData(0xe900,
                                        fontFamily: 'percent')),
                                    _disable),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                    child:
                        QuestionText(_questions[_questionIndex]['question'])),
                (_disabledAnswers.contains(0))
                    ? Answer(
                        null,
                        'A',
                        (_questions[_questionIndex]['answer']
                            as List<String>)[0])
                    : Answer(() {
                        _answer((_questions[_questionIndex]['answer']
                            as List<String>)[0]);
                      },
                        'A',
                        (_questions[_questionIndex]['answer']
                            as List<String>)[0]),
                (_disabledAnswers.contains(1))
                    ? Answer(
                        null,
                        'B',
                        (_questions[_questionIndex]['answer']
                            as List<String>)[1])
                    : Answer(() {
                        _answer((_questions[_questionIndex]['answer']
                            as List<String>)[1]);
                      },
                        'B',
                        (_questions[_questionIndex]['answer']
                            as List<String>)[1]),
                (_disabledAnswers.contains(2))
                    ? Answer(
                        null,
                        'C',
                        (_questions[_questionIndex]['answer']
                            as List<String>)[2])
                    : Answer(() {
                        _answer((_questions[_questionIndex]['answer']
                            as List<String>)[2]);
                      },
                        'C',
                        (_questions[_questionIndex]['answer']
                            as List<String>)[2]),
                (_disabledAnswers.contains(3))
                    ? Answer(
                        null,
                        'D',
                        (_questions[_questionIndex]['answer']
                            as List<String>)[3])
                    : Answer(() {
                        _answer((_questions[_questionIndex]['answer']
                            as List<String>)[3]);
                      },
                        'D',
                        (_questions[_questionIndex]['answer']
                            as List<String>)[3]),
                ResignButton(_resign),
              ],
            ),
          )
        : Result(prizes[12]);
  }
}
