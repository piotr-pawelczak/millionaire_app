import 'package:flutter/material.dart';

class HelpButton extends StatefulWidget {
  HelpButton({this.content, this.functionHandle});

  final Function functionHandle;
  final Icon content;

  @override
  HelpButtonState createState() {
    return HelpButtonState(
        content: this.content,
        functionHandle: this.functionHandle,
    );
  }
}

class HelpButtonState extends State<HelpButton> {
  HelpButtonState({this.content, this.functionHandle});

  final Icon content;
  final Function functionHandle;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
            color: Colors.indigo[900],
            textColor: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
                side: BorderSide(color: Colors.white)),
            child: content,
            onPressed: functionHandle,
          );
  }
}
