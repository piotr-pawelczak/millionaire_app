import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class AudienceAnswer extends StatelessWidget {

  final percentA;
  final percentB;
  final percentC;
  final percentD;

  AudienceAnswer(this.percentA, this.percentB, this.percentC, this.percentD);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.indigo[700],
      title: Text(
        'Ask the audience',
        style: TextStyle(color: Colors.white),
      ),
      actions: <Widget>[
        MaterialButton(
          elevation: 5.0,
          child: Text(
            'Back to game',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
      content: Container(
        height: 150,
        child: Column(
          children: <Widget>[
            LinearPercentIndicator(
              width: 200.0,
              lineHeight: 25.0,
              percent: percentA/100,
              backgroundColor: Colors.white,
              progressColor: Colors.blue,
              leading: Container(
                margin: EdgeInsets.only(right: 10),
                child: Text(
                  'A',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
              center: Text(
                '$percentA%',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 15),

            LinearPercentIndicator(
              width: 200.0,
              lineHeight: 25.0,
              percent: percentB/100,
              backgroundColor: Colors.white,
              progressColor: Colors.blue,
              leading: Container(
                margin: EdgeInsets.only(right: 10),
                child: Text(
                  'B',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
              center: Text(
                '$percentB%',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 15),

            LinearPercentIndicator(
              width: 200.0,
              lineHeight: 25.0,
              percent: percentC/100,
              backgroundColor: Colors.white,
              progressColor: Colors.blue,
              leading: Container(
                margin: EdgeInsets.only(right: 10),
                child: Text(
                  'C',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
              center: Text(
                '$percentC%',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 15),

            LinearPercentIndicator(
              width: 200.0,
              lineHeight: 25.0,
              percent: percentD/100,
              backgroundColor: Colors.white,
              progressColor: Colors.blue,
              leading: Container(
                margin: EdgeInsets.only(right: 10),
                child: Text(
                  'D',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
              center: Text(
                '$percentD%',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}