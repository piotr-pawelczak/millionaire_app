import 'package:flutter/material.dart';

class QuestionListElem extends StatelessWidget {
  final String content;
  final Color bgColor;


  QuestionListElem(this.content, this.bgColor);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: bgColor,
            border: Border.all(
                color: Colors.white
            ),
          ),
          padding: EdgeInsets.all(5),
          width: 250,
          child: Text(
            '$content',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        SizedBox(height: 2,)
      ],
    );
  }
}