import 'package:flutter/material.dart';
import './game.dart';
import './menu_button.dart';

void main() {
  runApp(MaterialApp(
    title: 'Who wants to be a Millionaire?',
    home: AppHome(),
    debugShowCheckedModeBanner: false,
  ));
}

class AppHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[700],
      appBar: AppBar(
        title: Center(
          child: Text('Who wants to be a Millionaire?'),
        ),
        backgroundColor: Colors.indigo[900],
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 30, bottom: 30),
            child: Image.network(
                'https://upload.wikimedia.org/wikipedia/en/thumb/4/4e/WWTBAMUS2020Logo.png/250px-WWTBAMUS2020Logo.png'),
          ),
          MenuButton('Start game', Game()),
        ],
      ),

    );
  }
}
