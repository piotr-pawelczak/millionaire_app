import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final Function selectHandler;
  final String answerText;
  final String label;

  Answer(this.selectHandler, this.label,  this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 0.9 * MediaQuery.of(context).size.width,
      child: RaisedButton(
        textColor: Colors.white,
        color: Colors.black,
        child: Row(
          children: <Widget>[
            Text(
                '$label:    ',
                style: TextStyle(color: Colors.orange),
            ),
            Text(
              answerText,
              style: TextStyle(fontSize: 15),
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.white)),
        onPressed: selectHandler,
      ),
    );
  }
}
