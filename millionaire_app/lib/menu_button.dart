import 'package:flutter/material.dart';

class MenuButton extends StatelessWidget {
  final Widget root;
  final String content;

  MenuButton(this.content, this.root);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        color: Colors.indigo[900],
        textColor: Colors.white,
        child: Text(
          content,
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => root),
          );
        },
      ),
    );
  }
}
