import 'package:flutter/material.dart';

class QuestionText extends StatelessWidget {

  final String text;
  QuestionText(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      width: 0.9 * MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.black,
        border: Border.all(
          color: Colors.white
        ),
      ),
      child: Center(
        child: Text(
            text,
          style: TextStyle(fontSize: 18, color: Colors.white),
        ),
      ),
    );
  }
}