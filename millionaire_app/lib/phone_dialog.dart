import 'package:flutter/material.dart';

class PhoneDialog extends StatelessWidget {

  final String phoneAnswer;
  PhoneDialog(this.phoneAnswer);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.indigo[700],
      title: Text(
          'Call friend...',
        style: TextStyle(color: Colors.white),
      ),
      content: Text(
          '$phoneAnswer',
          style: TextStyle(color: Colors.white),
      ),
      actions: <Widget>[
        MaterialButton(
          elevation: 5.0,
          child: Text(
              'Thanks!',
              style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
